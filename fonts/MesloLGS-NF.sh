#!/usr/bin/env bash

echo 'Installing Meslo Nerd Font'
font_dir="/usr/local/share/fonts/ttf/MesloLGS-NF"
mkdir --parents "${font_dir}"

if [ ! -f "${font_dir}/MesloLGS NF Regular.ttf" ]; then
  echo $'\t| Installing MesloLGS NF Regular...'
  curl --silent --location --output "${font_dir}/MesloLGS NF Regular.ttf" https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf
else
  echo $'\t| Already MesloLGS NF Regular installed'
fi
if [ ! -f "${font_dir}/MesloLGS NF Bold.ttf" ]; then
  echo $'\t| Installing MesloLGS NF Bold...'
  curl --silent --location --output "${font_dir}/MesloLGS NF Bold.ttf" https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf
else
  echo $'\t| Already MesloLGS NF Bold installed'
fi
if [ ! -f "${font_dir}/MesloLGS NF Italic.ttf" ]; then
  echo $'\t| Installing MesloLGS NF Italic...'
  curl --silent --location --output "${font_dir}/MesloLGS NF Italic.ttf" https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf
else
  echo $'\t| Already MesloLGS NF Italic installed'
fi
if [ ! -f "${font_dir}/MesloLGS NF Bold Italic.ttf" ]; then
  echo $'\t| Installing MesloLGS NF Bold Italic...'
  curl --silent --location --output "${font_dir}/MesloLGS NF Bold Italic.ttf" https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf
else
  echo $'\t| Already MesloLGS NF Bold Italic installed'
fi
echo $'\t| Rebuilding font cache...'
fc-cache
