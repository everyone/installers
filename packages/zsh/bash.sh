#!/bin/env bash
set -eu -o pipefail

tabs 2

user=${1:--1}
userId="$(id -u "${user}" 2>/dev/null || echo -1)"

[[ "${user}" == -1 ]] && {
  echo "No user was specified."
  exit 1
}

[[ "${userId}" == -1 ]] && {
  echo "Specified user does not exist."
  exit 1
}

[ -f "/etc/os-release" ] && source /etc/os-release
source "./utils/pkg.sh"

if [[ "${ID}" == "opensuse-microos" ]]; then
  pkg install zsh

  ./fonts/MesloLGS-NF.sh

  echo 'Installing global zshrc'
  # shellcheck disable=SC2002
  cat 'zsh/globalrc.zsh' | sudo tee /etc/zsh.zshrc.local 1>/dev/nul

  echo 'Creating Konsole profile with font enable'
  echo 'Enable font for vscode'
fi

tabs -0
