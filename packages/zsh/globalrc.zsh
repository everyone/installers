throw() {
        echo $@
        exit 2
}



ZSH_PLUGIN_DIR="/etc/zsh_plugin.d"

if [ ! -d "${ZSH_PLUGIN_DIR}" ]; then
        echo "ZSH plugin directory does not exists; creating."
        mkdir "${ZSH_PLUGIN_DIR}"
fi


download() {
        pluginName=$1
        url=$2
        [[ ! -n "$pluginName" || ! -n "$url" ]] && throw "Missing 1 or more arguments."

        mkdir -p "${ZSH_PLUGIN_DIR}/${pluginName}"

        curl -s "${url}" \
                | grep tarball_url \
                | cut -d'"' -f 4 \
                | wget -qi - -O - \
                | tar -xzf - -C "${ZSH_PLUGIN_DIR}/${pluginName}" --strip-components=1
}

sourcePlug() {
        pluginName="${1}"
        userRepo="${2}"
        [[ ! -n "$pluginName" || ! -n "$userRepo" ]] && throw "Missing 1 or more arguments."
        ext=`[[ $3 == "theme" ]] && echo "zsh-theme" || echo "plugin.zsh"`
        isPrivileged=`[[ $(print -P "%#") == '#' ]] && echo true || echo false`


        [[ -f "${ZSH_PLUGIN_DIR}/${pluginName}/${pluginName}.${ext}" ]] \
                && source "${ZSH_PLUGIN_DIR}/${pluginName}/${pluginName}.${ext}" \
                || {
                        source `download "${pluginName}" "https://api.github.com/repos/${userRepo}/tags" \
                                && echo "${ZSH_PLUGIN_DIR}/${pluginName}/${pluginName}.${ext}"`
                        }
}

alias userctl="systemctl --user"

# create plugin.d if not exist
test ! -d /etc/zsh_plugin.d && mkdir -vp /etc/zsh_plugin.d

sourcePlug powerlevel10k romkatv/powerlevel10k theme

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# sourcePlug zshrc freak2geek/zshrc
sourcePlug zsh-autosuggestions zsh-users/zsh-autosuggestions

ZSH_AUTOSUGGEST_STRATEGY=(history completion)

sourcePlug zsh-completions zsh-users/zsh-completions
sourcePlug fast-syntax-highlighting zdharma-continuum/fast-syntax-highlighting
sourcePlug zsh-history-substring-search zsh-users/zsh-history-substring-search

# Keybindings 

bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"

[[ -n "${key[Home]}" ]] && bindkey -- "${key[Home]}" beginning-of-line
[[ -n "${key[End]}" ]] && bindkey -- "${key[End]}" end-of-line
[[ -n "${key[Insert]}" ]] && bindkey -- "${key[Insert]}" overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}" backward-delete-char
[[ -n "${key[Delete]}" ]] && bindkey -- "${key[Delete]}" delete-char
[[ -n "${key[Up]}" ]] && bindkey -- "${key[Up]}" up-line-or-history
[[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}" down-line-or-history
[[ -n "${key[Left]}" ]] && bindkey -- "${key[Left]}" backward-char
[[ -n "${key[Right]}" ]] && bindkey -- "${key[Right]}" forward-char
[[ -n "${key[PageUp]}" ]] && bindkey -- "${key[PageUp]}" beginning-of-buffer-or-history
[[ -n "${key[PageDown]}" ]] && bindkey -- "${key[PageDown]}" end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}" reverse-menu-complete

if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
        autoload -Uz add-zle-hook-widget
        function zle_application_mode_start { echoti smkx }
        function zle_application_mode_stop { echoti rmkx }
        add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
        add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

key[Control-Left]="${terminfo[kLFT5]}"
key[Control-Right]="${terminfo[kRIT5]}"

[[ -n "${key[Control-Left]}" ]] && bindkey -- "${key[Control-Left]}" backward-word
[[ -n "${key[Control-Right]}" ]] && bindkey -- "${key[Control-Right]}" forward-word
