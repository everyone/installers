#!/bin/env bash
set -euo pipefail

eprintln() {
    cat <<<"$@" 1>&2
}

join_by() {
    local d=${1-} f=${2-}
    if shift 2; then
        printf %s "$f" "${@/#/$d}"
    fi
}

prompt() {
    variable="$1"
    shift
    default="$1"
    shift
    prompt="$1"
    shift
    requiments=("$@")

    [[ -z "${requiments[*]}" ]] && {
        eprintln "<prompt> requires an array of requirements"
        exit 1
    }

    if [[ "${requiments[0]}" == "null" ]]; then
        printf "\n%s\n" "$prompt"
        read -rep "> " -i "$default" "${variable?}"
    else
        # shellcheck disable=SC2086,SC2048
        printf "\n%s (Accepted values: %s)\n" "$prompt" "$(join_by ' | ' ${requiments[*]})"
        while :; do
            read -rep "> " -i "$default" "${variable?}"
            [[ "${requiments[0]}" == "null" ]] && break
            [[ -n ${!variable} && ${requiments[*]} =~ ${!variable} ]] && break
            eprintln "Invalid option\n"
        done
    fi
}

invert() {
    value=$1
    ((value ^= 1))
    echo $value
}

# Get required user info
## check boot mode
if [[ ! -f /sys/firmware/efi/fw_platform_size ]]; then
    eprintln "This script currently only supports UEFI systems."
    exit 1
fi
## Check Network Status
if [[ ! $(
    ping -c 1 archlinux.org
    invert $?
) ]]; then
    eprintln 'Connect to internet then rerun this script'
    exit 1
fi

## Disk info
mapfile -t devices < <(lsblk --paths --output NAME,SiZE --noheadings --nodeps)
paste <(printf "%s\n" "${!devices[@]}") <(printf "%s\n" "${devices[@]}")

prompt root_drive "" "Which device do you want to be root drive?" "${!devices[@]}"

prompt want_home "" "Do you want to store home on another drive?" 0 1

if [[ "${want_home?}" -eq 1 ]]; then
    prompt home_drive "" "Which device do you want to be home drive?" "${!devices[@]}"
    prompt home_fmt "" "Do you want to format home?" 0 1
fi

prompt want_swap "" "Do you want a swap space?" 0 1

if [[ "${want_swap?}" -eq 1 ]]; then
    prompt swap_size "2g" "Swap size. Default is 2g. (accepting k/m/g/e/p suffix)" null
fi

## root password
stty -echo
while :; do
    prompt password "" "Enter root password" null
    [[ "${password?}" == "" ]] && {
        eprintln "\nPassword cannot be empty"
        continue
    }
    prompt pass_conf "" "Confirm root password" null
    [[ "${pass_conf?}" == "${password?}" ]] && break
    eprintln "\nPassword does not match."
done
stty echo

# user confirmation
while :; do
    printf "\nThis script is descructive."
    prompt confirmed "" "Are you sure you want to contiue." YES NO
    [[ "${confirmed?}" == "YES" ]] && break
    [[ "${confirmed?}" == "NO" ]] && {
        printf "Exiting script\n"
        exit 1
    }
done

# Pre-installation
part_boot=",+1G,C12A7328-F81F-11D2-BA4B-00A0C93EC93B"
part_root=","
root_dev=$(echo ${devices[$root_drive]} | cut -d' ' -f 1)
[[ "${want_home:-0}" -eq 1 ]] && {
    (( $(lsblk ${devices[$home_drive]} | wc -l) > 3)) && parition=1
    home_dev=$(echo ${devices[$home_drive]} | cut -d' ' -f 1)${parition:-""}
    }

## Partition disk(s)
printf "label: GPT\n$part_boot\n$part_root" | sfdisk "$root_dev"
# [[ "${home_fmt:-0}" -eq 1 ]] &&
#     printf "label: GPT\n," | sfdisk "$home_dev"

## Format disk(s)
mkfs.fat -F 32 "$root_dev"1
mkfs.btrfs -f "$root_dev"2
[[ "${home_fmt:-0}" -eq 1 ]] && {
    mkfs.btrfs -f "$home_dev"
}

## Configure root btrfs
mount --mkdir "$root_dev"2 /mnt
btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@/.snapshots
mkdir /mnt/@/.snapshots/1
btrfs subvolume create /mnt/@/.snapshots/1/snapshot
mkdir -p /mnt/@/boot/grub2/
# btrfs subvolume create /mnt/@/boot/grub2/i386-pc
# btrfs subvolume create /mnt/@/boot/grub2/x86_64-efi
btrfs subvolume create /mnt/@/home
btrfs subvolume create /mnt/@/opt
btrfs subvolume create /mnt/@/root
btrfs subvolume create /mnt/@/srv
btrfs subvolume create /mnt/@/tmp
mkdir /mnt/@/usr/
btrfs subvolume create /mnt/@/usr/local
btrfs subvolume create /mnt/@/var
chattr +C /mnt/@/var

cat <<-'EOF' >>/mnt/@/.snapshots/1/info.xml
    <?xml version="1.0"?>
    <snapshot>
        <type>single</type>
        <num>1</num>
        <date>$DATE</date>
        <description>first root filesystem</description>
    </snapshot>
EOF

btrfs subvolume set-default "$(btrfs subvolume list /mnt | grep "@/.snapshots/1/snapshot" | grep -oP '(?<=ID )[0-9]+')" /mnt

umount /mnt

# Mount Disk(s)
mount --mkdir "$root_dev"2 /mnt
mount --mkdir "$root_dev"2 /mnt/.snapshots -o subvol=@/.snapshots
# mount --mkdir "$root_dev"2 /mnt/boot/grub2/i386-pc -o subvol=@/boot/grub2/i386-pc
# mount --mkdir "$root_dev"2 /mnt/boot/grub2/x86_64-efi -o subvol=@/boot/grub2/x86_64-efi
mount --mkdir "$root_dev"2 /mnt/home -o subvol=@/home
mount --mkdir "$root_dev"2 /mnt/opt -o subvol=@/opt
mount --mkdir "$root_dev"2 /mnt/root -o subvol=@/root
mount --mkdir "$root_dev"2 /mnt/srv -o subvol=@/srv
mount --mkdir "$root_dev"2 /mnt/tmp -o subvol=@/tmp
mount --mkdir "$root_dev"2 /mnt/usr/local -o subvol=@/usr/local
mount --mkdir "$root_dev"2 /mnt/var -o subvol=@/var
mount --mkdir -o uid=0,gid=0,fmask=0077,dmask=0077 "$root_dev"1 /mnt/boot
[[ "${want_home:-0}" -eq 1 ]] &&
    mount --mkdir "$home_dev" /mnt/home

### swap
[[ "${want_swap:-0}" -eq 1 ]] && {
    btrfs subvolume create /mnt/swap
    btrfs filesystem mkswapfile --uuid clear /mnt/swap/file --size "${swap_size?}"
    swapon /mnt/swap/file
}

# Installation
## Get and set mirrors in order of rate
reflector --save "/etc/pacman.d/mirrorlist" --protocol https,rsync --sort rate --threads 24 --latest 50
## install essential packages
pacstrap -K /mnt base linux-zen linux-firmware amd-ucode snapper snap-pac
# Configure Installation
## fstab
genfstab -U /mnt | tail -32 >>/mnt/etc/fstab
## chroot
### network configuration
printf "zesktop" >>/mnt/etc/hostname
cp -v /etc/systemd/network/* /mnt/etc/systemd/network/
arch-chroot /mnt bash <<CHROOT
systemctl enable systemd-networkd systemd-resolved

### time
ln -sf /usr/share/zoneinfo/America/Los_Angeles /etc/localtime
hwclock --systohc

### localization
sed -i 's/#en_US\\./en_US./' /etc/locale.gen
locale-gen
printf "LANG=en_US.UTF-8" >>/etc/locale.conf

### initramfs
sed --in-place 's/#COMPRESSION=\"zstd\"/COMPRESSION=\"zstd\"/' /etc/mkinitcpio.conf
mkinitcpio -P

### root password
printf "%s\n%s" "${password?}" "${password?}" | passwd

### Boot loader
bootctl install
cat << 'EOF' >/boot/loader/loader.conf
default  arch-zen.conf
timeout  4
console-mode max
editor   no
EOF
cat << 'EOF' >/boot/loader/entries/arch-zen.conf
title  Arch Linux Zen
linux   /vmlinuz-linux-zen
initrd  /amd-ucode.img
initrd  /initramfs-linux-zen.img
options root="UUID=$(blkid -s UUID -o value "$root_dev"2)" rw
EOF
mkdir -vp /etc/pacman.d/hooks/
cat << 'EOF' >/etc/pacman.d/hooks/95-systemd-boot.hook
[Trigger]
Type = Package
Operation = Upgrade
Target = systemd

[Action]
Description = Gracefully upgrading systemd-boot...
When = PostTransaction
Exec = /usr/bin/systemctl restart systemd-boot-update.service
EOF

### Setup snapper
umount /.snapshots
rm -rfv /.snapshots
snapper --no-dbus -c root create-config /
mount --mkdir "$root_dev"2 /.snapshots -o subvol=@/.snapshots
CHROOT

# Unmount disk(s) and reboot
# Post-installation???
