#!/usr/bin/env bash
[ -f "/etc/os-release" ] && . /etc/os-release

function pkg() {
  command=$1
  shift
  package=$1
  shift

  case "${command}" in
  install)
    if [[ "${ID}" == "opensuse-microos" ]]; then
      if [ ! "$(command -v "${package}")" ]; then
        sudo transactional-update --continue --drop-if-no-change run bash -c "zypper dup; zypper install ${package}"
      else
        echo "${package} is Already installed; Skipping."
      fi
    fi
    ;;
  *)
    echo "Command does not exist"
    exit 1
    ;;
  esac

}
